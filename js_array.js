/** If this array contains key, returns the index of
 * the first occurrence of key; otherwise returns -1. */
Array.prototype.linearSearch = function(key, compare) {
    if (typeof(compare) == 'undefined') {
        compare = ascend;
    }
    for (var i = 0;  i < this.length;  i++) {
        if (compare(this[i], key) == 0) {
            return i;
        }
    }
    return -1;
}
/** If this array contains key, returns the index of any
 * occurrence of key; otherwise returns -insertion - 1,
 * where insertion is the location within the array at
 * which the key should be inserted.  binarySearch assumes
 * this array is already sorted. */
Array.prototype.binarySearch = function(key, compare) {
    if (typeof(compare) == 'undefined') {
        compare = ascend;
    }
    var left = 0;
    var right = this.length - 1;
    while (left <= right) {
        var mid = left + ((right - left) >>> 1);
        var cmp = compare(key, this[mid]);
        if (cmp > 0)
            left = mid + 1;
        else if (cmp < 0)
            right = mid - 1;
        else
            return mid;
    }
    return -(left + 1);
}
/** Adds all the elements in the
 * specified arrays to this array. */
Array.prototype.addAll = function() {
    for (var a = 0;  a < arguments.length;  a++) {
        arr = arguments[a];
        for (var i = 0;  i < arr.length;  i++) {
            this.push(arr[i]);
        }
    }
}
/** Retains in this array all the elements
 * that are also found in the specified array. */
Array.prototype.retainAll = function(arr, compare) {
    if (typeof(compare) == 'undefined') {
        compare = ascend;
    }
    var i = 0;
    while (i < this.length) {
        if (arr.linearSearch(this[i], compare) == -1) {
            var end = i + 1;
            while (end < this.length &&
                    arr.linearSearch(this[end], compare) == -1) {
                end++;
            }
            this.splice(i, end - i);
        }
        else {
            i++;
        }
    }
}
/** Removes from this array all the elements
 * that are also found in the specified array. */
Array.prototype.removeAll = function(arr, compare) {
    if (typeof(compare) == 'undefined') {
        compare = ascend;
    }
    var i = 0;
    while (i < this.length) {
        if (arr.linearSearch(this[i], compare) != -1) {
            var end = i + 1;
            while (end < this.length &&
                    arr.linearSearch(this[end], compare) != -1) {
                end++;
            }
            this.splice(i, end - i);
        }
        else {
            i++;
        }
    }
}
/** Makes all elements in this array unique.  In other
 * words, removes all duplicate elements from this
 * array.  Assumes this array is already sorted. */
Array.prototype.unique = function(compare) {
    if (typeof(compare) == 'undefined') {
        compare = ascend;
    }
    var dst = 0;  // Destination for elements
    var src = 0;  // Source of elements
    var limit = this.length - 1;
    while (src < limit) {
        while (compare(this[src], this[src + 1]) == 0) {
            if (++src == limit) {
                break;
            }
        }
        this[dst++] = this[src++];
    }
    if (src == limit) {
        this[dst++] = this[src];
    }
    this.length = dst;
}
/** Compares two objects using
 * built-in JavaScript operators. */
function ascend(a, b) {
    if (a < b)
        return -1;
    else if (a > b)
        return 1;
    return 0;
}
function testArrays() {
    document.open();

    // Create the array listW.
    var listW = [ 'apple', 'pear', 'plum', 'peach' ];
    document.writeln('listW:  ' + listW + '<br>');

    // Use the linearSearch() function.
    document.writeln('linearSearch()<br>');
    var possible = 'plum';
    if (listW.linearSearch(possible) != -1)
        document.writeln('listW contains ' + possible);
    else
        document.writeln('listW does not contain ' + possible);
    possible = 'cherry';
    if (listW.linearSearch(possible) != -1)
        document.writeln('listW contains ' + possible + '<br>');
    else
        document.writeln('listW does not contain ' + possible + '<br>');

    // Use the addAll() function.
    var listX = [ 'cherry', 'banana', 'apricot', 'mango' ];
    listW.addAll(listX);
    listW.sort();
    document.writeln('addAll()<br>');
    document.writeln('listW:  ' + listW + '<br>');

    // Use the binarySearch() function.
    document.writeln('binarySearch()<br>');
    possible = 'peach';
    if (listW.binarySearch(possible) != -1)
        document.writeln('listW contains ' + possible + '<br>');
    else
        document.writeln('listW does not contain ' + possible + '<br>');
    possible = 'coconut';
    if (listW.binarySearch(possible) != -1)
        document.writeln('listW contains ' + possible + '<br>');
    else
        document.writeln('listW does not contain ' + possible + '<br>');
            
    // Use the addAll() function.
    var listY = [ 'elm', 'pine', 'rose', 'lilac' ];
    var listZ = [ 'lilac', 'pine', 'fir' ];
    listW.addAll(listY, listZ);
    document.writeln('addAll()<br>');
    document.writeln('listY:  ' + listY + '<br>');
    document.writeln('listZ:  ' + listZ + '<br>');
    document.writeln('listW:  ' + listW + '<br>');

    // Use the retainAll() function.
    listY.retainAll(listZ);
    document.writeln('retainAll()<br>');
    document.writeln('listY:  ' + listY + '<br>');

    // Use the removeAll() function.
    listY = [ 'elm', 'pine', 'rose', 'lilac' ];
    listY.removeAll(listZ);
    document.writeln('removeAll()<br>');
    document.writeln('listY:  ' + listY + '<br>');

    // Use the unique() function.
    listW.addAll(listX, listY, listZ);
    listW.sort();
    listW.unique();
    document.writeln('unique()<br>');
    document.writeln('listW:  ' + listW + '<br>');

    document.close();