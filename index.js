const Discord = require("discord.js");


const client = new Discord.Client();

// load the config.json file that contains our token and our prefix values.
const config = require("./config.json");
// config.token contains the bot's token
// config.prefix contains the message prefix.

client.on("ready", () => {
  console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`);
  //client.user.setActivity(`Serving ${client.guilds.size} servers`);
  client.user.setActivity(`hivebot.gg`);
});

client.on("guildCreate", guild => {
  console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
  //client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

client.on("guildDelete", guild => {
  console.log(`I have been removed from: ${guild.name} (id: ${guild.id})`);
  //client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

client.on("message", async message => {
  if (message.channel.type == "dm") return; //if channel is dm return
  if (message.author.bot) return; //if author is bot dont do anything
  if (message.content.indexOf(config.prefix) !== 0) {

    //REGION comparingstrings

    let cschannel = await message.guild.channels.find(c => c.name == "comparingstrings");
    if (cschannel != null) {
      //.clean is a protoype on array at the end of file


      let x = message.content.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, " ");
      let xWords = x.toLowerCase().split(" ").clean("");
      cschannel.messages.forEach(function(msg) {
        let y = msg.content.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, " ");
        let yWords = y.toLowerCase().split(" ").clean("");

        const intersection = xWords.filter(element => yWords.includes(element));
        console.log((intersection.length * 100 / xWords.length) + "% de mot identique avec l'ancien feedback");
      });



    }
    //ENDREGION
    let feedbackChannel = await message.guild.channels.find(c => c.name == "feedbacks");
    let bestvotedfeedbackChannel = await message.guild.channels.find(c => c.name == "les-plus-votés");

    if (feedbackChannel != null & bestvotedfeedbackChannel != null) {
      if (message.channel.id == feedbackChannel.id) {
        message.channel.send({
            embed: {
              color: 6881467,
              description: "Before posting this feedback, do you confirm you read the rules ? if the feedback is out of rules, it will be deleted and not recoverable",
              fields: [{
                  name: `Your feedback`,
                  value: message.content
                },
                {
                  name: `💚 to confirm`,
                  value: `Your feedback will be posted`
                },
                {
                  name: `🔴 to deny`,
                  value: `Your feedback will be deleted and sent in your PM to be recoverable`
                }
              ],
            }
          })
          .then(async confirmation => {
            await confirmation.react('💚');
            await confirmation.react('🔴');

            // Create a reaction collector
            const filter = (reaction, user) => (reaction.emoji.name === '💚' || reaction.emoji.name === '🔴') && user.id != confirmation.author.id;
            const collector = confirmation.createReactionCollector(filter, {
              time: 15000
            });

            collector.on('collect', r => {
              if (r.emoji.name === '💚') { // YES
                confirmation.delete();
                message.delete();

                message.channel.send({
                  embed: {
                    color: 6881467,
                    author: {
                      name: message.author.username,
                      icon_url: message.author.avatarURL
                    },
                    description: message.content,
                  }
                }).then(async feedbackMessage => {
                  await feedbackMessage.react('👍');
                  await feedbackMessage.react('👎');
                  await feedbackMessage.react('⏫');
                })

              } else if (r.emoji.name === '🔴') { // NO
                confirmation.delete();
                message.author.send({
                  embed: {
                    color: 6881467,
                    description: "Please read the rules before you send a feedback.",
                    fields: [{
                      name: 'Your feedback :',
                      value: message.content
                    }]
                  }
                })
                message.delete();
              }
            });
            collector.on('end', collected => {
              if (!confirmation.deleted) {
                confirmation.delete();
                message.author.send({
                  embed: {
                    color: 6881467,
                    description: "You took too long to confirm your feedback",
                    fields: [{
                      name: 'Your feedback :',
                      value: message.content
                    }]
                  }
                })
                message.delete();
              }
            });
          })

      }
    }



  } else {

    //separate "command", and "arguments"
    const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();



    //command part
    if (command === "feedback") {
      if (args.length <= 0) {
        message.channel.send({
          embed: {
            color: 6881467,
            description: "List of command available",
            fields: [{
              name: "#feedback",
              value: "Display the list of command"
            }],
          }
        });
      } else {
        switch (args[0].toLowerCase()) {

          case "info":
            let feedbackChannel = await message.guild.channels.find(c => c.name == "feedbacks");
            if (feedbackChannel == null) {
              message.channel.send({
                embed: {
                  color: 6881467,
                  description: "the Feedback channels doesn't exist",
                }
              });
            } else {
              //gather information


              //display them
              message.channel.send({
                embed: {
                  color: 6881467,
                  fields: [{
                    name: `Number of feedback :`,
                    value: `OVER THAN NINE THOUSAAAAAANNNNND`
                  }],
                }
              });
            }

            break;

          default:
            message.channel.send({
              embed: {
                color: 6881467,
                description: "this command is not available, try '#feedback' to see all command",
              }
            });
            break;
        }
      }

    }
  };

});

client.on('messageReactionAdd', (reaction, user) => {
  if (reaction.message.guild.channels.find(c => c.name == "les-plus-votés") == null) return;
  if (reaction.message.guild.channels.find(c => c.name == "feedbacks") == null) return;
  if (reaction.message.channel.id == reaction.message.guild.channels.find(c => c.name == "feedbacks").id) {

    switch (reaction._emoji.name) {
      case "👍":
        if (reaction.count > 1) {

          //transfer the message to les-plus-votés
          let topChannel = reaction.message.guild.channels.find(c => c.name == "les-plus-votés");
          topChannel.send({
            embed: {
              color: 6881467,
              author: {
                name: user.username,
                icon_url: "https://cdn.discordapp.com/avatars/" + user.id + "/" + user.avatar + ".png"
              },
              description: reaction.message.embeds[0].description,
            }
          }).then(async feedbackMessage => {
            feedbackMessage.react('⏫');
          })

          //send a message to user to notify the transfer
          let author = reaction.message.member.guild.members.get(reaction.message.embeds[0].author.iconURL.split("/")[4]);
          author.send({
            embed: {
              color: 6881467,
              description: "Congratulations! your feedback have a good succes, he is now in the les-plus-votés channel, you can react with a ❤️ to help him getting higher",
              fields: [{
                name: "your feedback :",
                value: reaction.message.embeds[0].description
              }]
            }
          })

          //delete the message in feedback channel
          reaction.message.delete();
        }
        console.log("UP");
        break;
      case "👎":
        if (reaction.count > 1) {

        }
        console.log("DOWN");
        break;
      case "⏫":
        if (reaction.count > 1) {

        }
        console.log("OLD");
        break;
    }
  }
});

//https://github.com/AnIdiotsGuide/discordjs-bot-guide/blob/master/coding-guides/raw-events.md
client.on('raw', packet => {
  // We don't want this to run on unrelated packets
  if (!['MESSAGE_REACTION_ADD', 'MESSAGE_REACTION_REMOVE'].includes(packet.t)) return;
  // Grab the channel to check the message from
  const channel = client.channels.get(packet.d.channel_id);
  // There's no need to emit if the message is cached, because the event will fire anyway for that
  if (channel.messages.has(packet.d.message_id)) return;
  // Since we have confirmed the message is not cached, let's fetch it
  channel.fetchMessage(packet.d.message_id).then(message => {
    // Emojis can have identifiers of name:id format, so we have to account for that case as well
    const emoji = packet.d.emoji.id ? `${packet.d.emoji.name}:${packet.d.emoji.id}` : packet.d.emoji.name;
    // This gives us the reaction we need to emit the event properly, in top of the message object
    const reaction = message.reactions.get(emoji);
    // Adds the currently reacting user to the reaction's users collection.
    if (reaction) reaction.users.set(packet.d.user_id, client.users.get(packet.d.user_id));
    // Check which type of event it is before emitting
    if (packet.t === 'MESSAGE_REACTION_ADD') {
      client.emit('messageReactionAdd', reaction, client.users.get(packet.d.user_id));
    }
    if (packet.t === 'MESSAGE_REACTION_REMOVE') {
      client.emit('messageReactionRemove', reaction, client.users.get(packet.d.user_id));
    }
  });
});


//ARRAY clean
Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};


client.login(config.token);
